package profile.profile_management.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import profile.profile_management.dto.LoginRequest;
import profile.profile_management.dto.LoginResponse;
import profile.profile_management.dto.PasswordDto;
import profile.profile_management.dto.PasswordResponseDto;
import profile.profile_management.model.Users;
import profile.profile_management.security.JWTUtil;
import profile.profile_management.security.PBKDF2Encoder;
import profile.profile_management.service.UserService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ProfileController {

    private final UserService userService;
    private final PBKDF2Encoder passwordEncoder;
    private final JWTUtil jwtUtil;

    private static final String emailRegex = "^(.+)@(.+)$";
    private static final String passwordRegex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=.*[@#$%^&+=])" + "(?=\\S+$).{8,20}$";

    @GetMapping(value ="/users", produces = MediaType. TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<Users> getAllUsers(){
        return userService.findUsers();
    }

    @GetMapping(value ="/user/{id}", produces = MediaType. TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<Users> getUserById(@PathVariable long id){
        return userService.findUserEntityById(id);
    }

    @PostMapping(value = "/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ResponseEntity<PasswordResponseDto>> addUser(@RequestBody Users user){
        Pattern emailPattern = Pattern.compile(emailRegex);
        Pattern passwordPattern = Pattern.compile(passwordRegex);
        Matcher matcher = emailPattern.matcher(user.getEmail());
        Matcher passMatcher = passwordPattern.matcher(user.getPassword());
        if(!matcher.matches()){
            return Mono.just(ResponseEntity.ok(new PasswordResponseDto("Email is invalid")));
        }
        if(!passMatcher.matches()){
            return Mono.just(ResponseEntity.ok(new PasswordResponseDto("Password is invalid")));
        }
        return userService.createUser(user)
                .map(userEntity -> ResponseEntity.ok(new PasswordResponseDto("User registered successfully")));
    }

    @PutMapping(value = "/update/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Users> updateUser(@RequestBody Users user, @PathVariable long id){
        return userService.updateUser(id, user);
    }


    @PostMapping("/login")
    public Mono<ResponseEntity<LoginResponse>> login(@RequestBody LoginRequest ar) {
        return userService.findUserEntityByEmail(ar.getUsername())
                .filter(userDetails -> passwordEncoder.encode(ar.getPassword()).equals(userDetails.getPassword()))
                .map(userDetails -> ResponseEntity.ok(new LoginResponse(jwtUtil.generateToken(userDetails))))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()));
    }
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> deleteUser(@PathVariable long id){
        return userService.deleteUser(id);
    }
    @PostMapping("/change/credential")
    public Mono<ResponseEntity<PasswordResponseDto>> changeCredentials(@RequestBody PasswordDto credentialDto){
        return userService.changeCredentials(credentialDto)
                .map(userEntity -> ResponseEntity.ok(new PasswordResponseDto("Credential changed successfully")))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.BAD_REQUEST).build()));
    }
}
