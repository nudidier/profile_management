package profile.profile_management.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="users")
public class Users {
    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private Boolean isActive;
    private String email;
    private String password;
}
