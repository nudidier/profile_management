package profile.profile_management.service;


import profile.profile_management.dto.PasswordDto;
import profile.profile_management.model.Users;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
   Mono<Users> findUserEntityByEmail(String email);
   Mono<Users> findUserEntityById(long id);
   Flux<Users> findUsers();
   Mono<Users> createUser(Users user);
   Mono<Users> updateUser(long id, Users user);

   Mono<Users> blockUser(Users user);
   Mono<Void> deleteUser(long id);
   Mono<Users> changeCredentials(PasswordDto credentialDto);

}
