package profile.profile_management.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import profile.profile_management.model.Users;
import reactor.core.publisher.Mono;

@Repository
public interface UsersRepository extends ReactiveCrudRepository<Users, Long> {
    Mono<Users> findUserEntityByEmail(String email);
}
