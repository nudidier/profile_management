package profile.profile_management.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import profile.profile_management.dto.PasswordDto;
import profile.profile_management.model.Users;
import profile.profile_management.repository.UsersRepository;
import profile.profile_management.security.PBKDF2Encoder;
import profile.profile_management.service.UserService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UsersRepository userEntityRepository;
    private final PBKDF2Encoder passwordEncoder;

    public Mono<Users> findUserEntityByEmail(String email) {
        return userEntityRepository.findUserEntityByEmail(email).switchIfEmpty(Mono.error(new RuntimeException("User not found")));
    }

    @Override
    public Mono<Users> findUserEntityById(long id) {
        return userEntityRepository.findById(id).switchIfEmpty(Mono.error(new RuntimeException("User not found")));
    }

    public Flux<Users> findUsers() {
        return userEntityRepository.findAll();
    }

    public Mono<Users> createUser(Users user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userEntityRepository.save(user);
    }

    @Override
    public Mono<Users> updateUser(long id, Users user) {
        return userEntityRepository.findById(user.getId())
                .switchIfEmpty(Mono.error(new RuntimeException("User not found")))
                .map(olderUser -> {
                    if(user.getFirstName() == null){
                        olderUser.setFirstName(user.getFirstName());
                    }else if(user.getLastName() == null){
                        olderUser.setLastName(user.getLastName());
                    }else if(user.getEmail() == null){
                        olderUser.setEmail(user.getEmail());
                    }else if(user.getPassword() == null){
                        olderUser.setPassword(passwordEncoder.encode(user.getPassword()));
                    }
                    return olderUser;
                })
                .flatMap(userEntityRepository::save);
    }

    public Mono<Users> blockUser(Users user){
        return userEntityRepository.findById(user.getId())
                .switchIfEmpty(Mono.error(new RuntimeException("User not found")))
                .map(olderUser -> {
                            olderUser.setIsActive(false);
                            return olderUser;
                        })
                .flatMap(userEntityRepository::save);
    }

    @Override
    public Mono<Void> deleteUser(long id) {
        return userEntityRepository.deleteById(id);
    }

    @Override
    public Mono<Users> changeCredentials(PasswordDto credentialDto) {
        return userEntityRepository.findUserEntityByEmail(credentialDto.getEmail())
                .switchIfEmpty(Mono.error(new RuntimeException("User not found")))
                .map(foundUser -> {
                    foundUser.setPassword(passwordEncoder.encode(credentialDto.getPassword()));
                    return foundUser;
                })
                .flatMap(userEntityRepository::save);
    }
}
